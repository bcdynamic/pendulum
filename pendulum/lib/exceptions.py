class LibraryException(Exception):
    pass


class TogglException(LibraryException):
    pass
