import datetime
import json
import math
import re

import dateutil.parser
import pytz
import requests

from pendulum.lib import exceptions

URL = 'https://www.toggl.com/api/v8/time_entries'
HEADERS = {'Content-Type': 'application/json'}
TZ_INFO = pytz.timezone('America/Vancouver')
POSTED_JIRA_TAG = 'Logged'
POSTED_FRESHDESK_TAG = '1-Freshdesk'
BILLABLE_TAG = '2-Billable'
SUPPORT_ISSUE = 'ADM-11'
PATTERN = re.compile('(?P<issue>[A-Z]{2,3}\-[0-9]+)\s?\[?(?P<ticket_number>[0-9]+)?\]?\s?(?P<comment>.*)')


class TimeEntry:
    def __init__(self, id, wid, billable, start, duration, duronly, at, uid, description='', pid=None, tags=None, guid=None, stop=None, freshdesk_enabled=True):
        self.id = id
        self.pid = pid
        self.wid = wid
        self.billable = billable
        self.start = start
        self.stop = stop
        self.duration = duration
        self.description = description
        self.duronly = duronly
        self.at = at
        self.uid = uid
        self.tags = tags or []
        self.freshdesk_enabled = freshdesk_enabled

    def __repr__(self):
        return self.description

    @property
    def postable(self):
        return self.duration != -1

    @property
    def posted_jira(self):
        return POSTED_JIRA_TAG in self.tags

    @property
    def posted_freshdesk(self):
        return POSTED_FRESHDESK_TAG in self.tags

    @property
    def is_billable(self):
        return BILLABLE_TAG in self.tags

    @property
    def match(self):
        match = PATTERN.match(self.description.strip())
        if not match:
            raise exceptions.TogglException('Unable to parse entry {entry}'.format(entry=self))
        return match

    @property
    def issue(self):
        return self.match.group('issue')

    @property
    def ticket_number(self):
        return self.match.group('ticket_number')

    @property
    def comment(self):
        return self.match.group('comment')

    @property
    def is_admin(self):
        return self.issue.startswith('ADM')

    @property
    def is_support(self):
        return self.issue == SUPPORT_ISSUE

    @property
    def started(self):
        return dateutil.parser.parse(self.start)

    @property
    def running(self):
        return self.duration < 0

    @property
    def time_in_seconds(self):
        return round(self.duration / 60) * 60 or 60

    @property
    def hours(self):
        return '{:02.0f}'.format(math.floor(self.time_in_seconds / 3600))

    @property
    def minutes(self):
        return '{:02.0f}'.format(self.time_in_seconds % 3600 / 60)

    @property
    def freshdesk_time(self):
        return '{}:{}'.format(self.hours, self.minutes)

    @property
    def printable_time(self):
        return '{}h {}m'.format(self.hours, self.minutes)

    @property
    def for_freshdesk(self):
        if self.freshdesk_enabled:
            return bool(self.ticket_number)
        return False

    @property
    def needs_posting(self):
        if self.for_freshdesk and not self.posted_freshdesk:
            return True
        return not self.posted_jira

    @property
    def jira(self):
        return {
            'issue': self.issue,
            'started': self.started,
            'timeSpentSeconds': self.time_in_seconds,
            'comment': self.comment,
        }

    @property
    def freshdesk(self):
        return {
            'ticket_id': self.ticket_number,
            'note': self.comment,
            'time_spent': self.freshdesk_time,
            'start_time': self.start,
            'billable': self.is_billable,
        }

    def mark_jira_posted(self):
        if not self.posted_jira:
            self.tags.append(POSTED_JIRA_TAG)

    def mark_freshdesk_posted(self):
        if not self.posted_freshdesk:
            self.tags.append(POSTED_FRESHDESK_TAG)


class Toggl:
    def __init__(self, token, workspace_id, freshdesk_enabled=True):
        self.token = token
        self.workspace_id = workspace_id
        self.freshdesk_enabled = freshdesk_enabled

    @property
    def auth(self):
        return (self.token, 'api_token')

    @property
    def params(self):
        return {
            'headers': HEADERS,
            'auth': self.auth,
        }

    def get_entries(self, date=datetime.datetime.now(tz=TZ_INFO)):
        # Beginning of day
        start_date = TZ_INFO.localize(datetime.datetime(date.year, date.month, date.day, 0, 0, 0))
        response = requests.get(
            url=URL,
            params={
                'start_date': start_date.isoformat(),  # (date - datetime.timedelta(days=1)).isoformat(),
                'end_date': date.isoformat(),
            },
            **self.params
        )
        try:
            return [TimeEntry(freshdesk_enabled=self.freshdesk_enabled, **entry) for entry in response.json()]
        except json.decoder.JSONDecodeError:
            raise exceptions.TogglException(response.content)

    def update_entry(self, entry):
        response = requests.put(
            url='{url}/{id}'.format(url=URL, id=entry.id),
            data=json.dumps({'time_entry': entry.__dict__}),
            **self.params
        )
        response.raise_for_status()
        return response.content
